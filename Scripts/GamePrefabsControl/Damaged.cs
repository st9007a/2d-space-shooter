﻿using UnityEngine;
using System.Collections;

public class Damaged : MonoBehaviour {

    public int health = 1;
    public float invulnPeriod = 0;

    float invulnTimer = 0;
    int correctLayer;
    static int killCount = 0;
    SpriteRenderer spriteRend;

	// Use this for initialization
	void Start () {
        correctLayer = gameObject.layer;
        spriteRend = GetComponent<SpriteRenderer>();

        if (spriteRend == null) {
            spriteRend = transform.GetComponentInChildren<SpriteRenderer>();

            if (spriteRend == null) { 
                Debug.LogError("Object '" + gameObject.name + "' has no sprite render");
            }    
        }
	}

    void OnTriggerEnter2D() {

        health--;

        if (invulnPeriod > 0){
            invulnTimer = invulnPeriod;
            gameObject.layer = 12;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (invulnTimer > 0){
            invulnTimer -= Time.deltaTime;

            if (invulnTimer <= 0)
            {
                gameObject.layer = correctLayer;

                if (spriteRend != null)
                {
                    spriteRend.enabled = true;
                }
            }
            else
            {
                if (spriteRend != null)
                {
                    spriteRend.enabled = !spriteRend.enabled;
                }
            }
        }

        if (health <= 0){
            if (gameObject.layer == 10) {
                killCount++;
            }
            Die();
        }
	}

    void Die() {
        Destroy(gameObject);       
    }

    void OnGUI()
    {
       
        GUI.Label(new Rect(0, 20, 100, 50), "Kill : " + killCount);
        
        
    }
}
