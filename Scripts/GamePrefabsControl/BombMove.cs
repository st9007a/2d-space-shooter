﻿using UnityEngine;
using System.Collections;

public class BombMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        float axisY;

        axisY = Mathf.Lerp(transform.position.y, 0, 0.01f);
        transform.position = new Vector3(0, axisY, 0);
	
	}
}
