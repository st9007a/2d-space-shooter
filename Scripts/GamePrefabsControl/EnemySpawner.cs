﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {


    public GameObject enemyPrefab;
    public GameObject parents;
    public GameObject bomb;

    public float bombDelay = 10f;

    float spawnDistance = 12f;
    float enemyRate = 5f;
    float nextEnemy = 1f;
    float bombTime = 10f;

	
	// Update is called once per frame
	void Update () {
        nextEnemy -= Time.deltaTime;
        enemyRate *= 0.9f;
        if (enemyRate < 2) {
            enemyRate = 2;
        }

        if (nextEnemy <= 0) {
            nextEnemy = enemyRate;

            Vector3 offset = Random.onUnitSphere;

            offset.z = 0;
            offset = offset.normalized * spawnDistance;
            

            GameObject check = GameObject.FindGameObjectWithTag("ui_start");
            
            if (check != null) {
                Transform clone;
                clone = Instantiate(enemyPrefab.transform, transform.position + offset, Quaternion.identity) as Transform;
                clone.transform.parent = parents.transform;
            }
            else
            {
                Instantiate(enemyPrefab, transform.position + offset, Quaternion.identity);
       
            }
           
        }
	
	}

    void FixedUpdate (){
        GameObject check = GameObject.FindGameObjectWithTag("ui_start");

        if(check == null)
        {
            
            bombTime -= Time.deltaTime;
            if (bombTime <= 0)
            {
                
                Instantiate(bomb, new Vector3(0, 20f, 0), Quaternion.identity);
                
                bombTime = bombDelay;
            }
        }

    }
}
