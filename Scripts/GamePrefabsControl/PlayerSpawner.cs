﻿using UnityEngine;
using System.Collections;

public class PlayerSpawner : MonoBehaviour {

    public GameObject playerPrefab;
    public GameObject retry;
    public int numLive = 4;

    GameObject playerInstance;
    float respawnTimer = 2.0f;

	// Use this for initialization
	void Start () {
        spawnPlayer();
	}

    void spawnPlayer() {
        numLive--;
        respawnTimer = 1;
        playerInstance = (GameObject)Instantiate(playerPrefab, transform.position, Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {

        if (playerInstance == null && numLive > 0) {
            respawnTimer -= Time.deltaTime;
            if (respawnTimer <= 0) {
                spawnPlayer();
            }
        }
        if (numLive <= 0 && playerInstance == null) {
            GameObject easy = GameObject.FindGameObjectWithTag("easy");
            GameObject hard = GameObject.FindGameObjectWithTag("hard");
            if (easy != null) {
                Destroy(easy);
            }
            if (hard != null) {
                Destroy(hard);
            }
            
            retry.SetActive(true);   //無法運作???
            Destroy(gameObject);
        }
	}

    void OnGUI() {
        if (numLive > 0 || playerInstance != null){
            GUI.Label(new Rect(0, 0, 100, 50), "Lives Left: " + numLive);
        }
        else {
            GUI.Label(new Rect(Screen.width/2 - 50, Screen.height/2 - 25, 100, 50), "Game Over");
        }
    }
}
