﻿using UnityEngine;
using System.Collections;

public class BombDestruct : MonoBehaviour {


    public float lifeTime = 7.5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        lifeTime -= Time.deltaTime;

        if(lifeTime <= 0)
        {
            Destroy(gameObject);
        }
	
	}
}
