﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    float movespeed = 5.0f;
    float rotspeed = 180f;
    float ShipRadius = 0.5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


        //ship move and rotato
        Quaternion rot = transform.rotation;
        float z = rot.eulerAngles.z;
        z -= Input.GetAxis("Horizontal") * rotspeed * Time.deltaTime;
        rot = Quaternion.Euler(0,0,z);
        transform.rotation = rot;

        Vector3 pos = transform.position;
        Vector3 velocity =new Vector3(0, Input.GetAxis("Vertical") * movespeed * Time.deltaTime ,0);
        pos += rot * velocity;

        //limit in screen
        if (pos.y + ShipRadius > Camera.main.orthographicSize) {
            pos.y = Camera.main.orthographicSize - ShipRadius;
        }
        if (pos.y - ShipRadius < -Camera.main.orthographicSize) {
            pos.y = -Camera.main.orthographicSize + ShipRadius;
        }

        float screenRatio = (float)Screen.width / (float)Screen.height;
        float screenWidth = screenRatio * Camera.main.orthographicSize;

        if (pos.x + ShipRadius > screenWidth) {
            pos.x = screenWidth - ShipRadius;
        }
        if (pos.x - ShipRadius < -screenWidth) {
            pos.x = -screenWidth + ShipRadius;
        }

        //ship position
        transform.position = pos;
	
	}
}
