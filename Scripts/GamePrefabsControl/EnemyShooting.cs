﻿using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour {

    public GameObject bulletPrefab;
    public Vector3 bulletOffset = new Vector3(0, 0.7f, 0);
    public float fireDelay = 0.5f;

    int bulletLayer;
    float cooldownTimer = 0;
    Transform player;

    void Start () {
        bulletLayer = gameObject.layer;
    }

    // Update is called once per frame
    void Update() {

        if (player == null) {
            GameObject go = GameObject.FindWithTag("Player");

            if (go != null){
                player = go.transform;
            }
        }

        cooldownTimer -= Time.deltaTime;

        if (cooldownTimer <= 0 && player != null && Vector3.Distance(transform.position,player.position) < 4 )
        {
            //shoot
            cooldownTimer = fireDelay;

            Vector3 offset = transform.rotation * bulletOffset;

            GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, transform.position + offset, transform.rotation);
            bulletGO.layer = bulletLayer;
        }

    }
}
