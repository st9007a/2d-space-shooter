﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour {

    public GameObject bulletPrefab;
    public Vector3 bulletOffset = new Vector3(0, 0.7f, 0);
    public float fireDelay = 0.25f;

    int bulletLayer;
    float cooldownTimer = 0;

    void Start () {
        bulletLayer = gameObject.layer;
    }
	
	// Update is called once per frame
	void Update () {
        cooldownTimer -= Time.deltaTime;
        
        if (Input.GetButtonDown("Fire1") && cooldownTimer <= 0){

            cooldownTimer = fireDelay;

            Vector3 offset = transform.rotation * bulletOffset;

            GameObject bulletGO1 = (GameObject)Instantiate(bulletPrefab, transform.position + offset, transform.rotation);
            GameObject bulletGO2 = (GameObject)Instantiate(bulletPrefab, transform.position + offset, transform.rotation * Quaternion.Euler(0, 0, 30));
            GameObject bulletGO3 = (GameObject)Instantiate(bulletPrefab, transform.position + offset, transform.rotation * Quaternion.Euler(0, 0, -30));
            bulletGO1.layer = bulletLayer;
            bulletGO2.layer = bulletLayer;
            bulletGO3.layer = bulletLayer;
        }
	
	}
}
