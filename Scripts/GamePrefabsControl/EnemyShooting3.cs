﻿using UnityEngine;
using System.Collections;

public class EnemyShooting3 : MonoBehaviour {


    public GameObject bulletPrefab;
    public float fireDelay = 0.5f;

    int bulletLayer;
    float cooldownTimer = 0;
    

    // Use this for initialization
    void Start () {

        bulletLayer = gameObject.layer;

    }
	
	// Update is called once per frame
	void Update () {


        cooldownTimer -= Time.deltaTime;
        if (cooldownTimer <= 0)
        {

            cooldownTimer = fireDelay;

            GameObject bulletGO1 = (GameObject)Instantiate(bulletPrefab, transform.position, transform.rotation);
            GameObject bulletGO2 = (GameObject)Instantiate(bulletPrefab, transform.position, transform.rotation * Quaternion.Euler(0, 0, 60));
            GameObject bulletGO3 = (GameObject)Instantiate(bulletPrefab, transform.position, transform.rotation * Quaternion.Euler(0, 0, 120));
            GameObject bulletGO4 = (GameObject)Instantiate(bulletPrefab, transform.position, transform.rotation * Quaternion.Euler(0, 0, 180));
            GameObject bulletGO5 = (GameObject)Instantiate(bulletPrefab, transform.position, transform.rotation * Quaternion.Euler(0, 0, 240));
            GameObject bulletGO6 = (GameObject)Instantiate(bulletPrefab, transform.position, transform.rotation * Quaternion.Euler(0, 0, 300));
            bulletGO1.layer = bulletLayer;
            bulletGO2.layer = bulletLayer;
            bulletGO3.layer = bulletLayer;
            bulletGO4.layer = bulletLayer;
            bulletGO5.layer = bulletLayer;
            bulletGO6.layer = bulletLayer;
        }

    }
}
