﻿using UnityEngine;
using System.Collections;

public class ModeEasy : MonoBehaviour {

    public GameObject background;
    
    public GameObject PlayerShip;             //playerSpawner
    public GameObject parent;                 //自己

    public GameObject Timer;

    //public GameObject hp;

    public int setMode;

    public void click()
    {
        if (setMode == 1) {
            GameObject hard = GameObject.FindGameObjectWithTag("hard");    
            Destroy(hard);
        }
        if(setMode == 2){
            GameObject easy = GameObject.FindGameObjectWithTag("easy");
            Destroy(easy);
        }
       
        Destroy(background);

        Instantiate(PlayerShip,new Vector3(0,0,0),Quaternion.identity);

        Timer.SetActive(true);
        //hp.SetActive(true);
        
        parent.SetActive(false);
    }
}
