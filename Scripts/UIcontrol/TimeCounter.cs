﻿using UnityEngine;
using System.Collections;
using System;

public class TimeCounter : MonoBehaviour {

    public UILabel timeLabel;
    public GameObject timeObject;
    string timeText;
    float time;

    // Use this for initialization
    void Start() {
        timeObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {

        time += Time.deltaTime;
        TimeSpan timeSpan = TimeSpan.FromSeconds(time);

        timeText = string.Format("{0:D2}:{1:D2}:{2:D2}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
        timeLabel.text = timeText;
	
	}
}
