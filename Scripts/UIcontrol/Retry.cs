﻿using UnityEngine;
using System.Collections;

public class Retry : MonoBehaviour {


    public GameObject chooseMode;
    public GameObject background;
    public GameObject easy;
    public GameObject hard;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void click() {
        GameObject[] a = GameObject.FindGameObjectsWithTag("EnemyHard");
        GameObject[] b = GameObject.FindGameObjectsWithTag("EnemyEasy");

        if (a != null)
        {
            for (int i = 0; i < a.Length; i++)
                Destroy(a[i]);
        }
        if (b != null)
        {
            for (int i = 0; i < b.Length; i++)
                Destroy(b[i]);
        }

        chooseMode.SetActive(true);
        Instantiate(background, new Vector3(0, 0, 0), Quaternion.identity);
        Instantiate(easy, new Vector3(0, 0, 0), Quaternion.identity);
        Instantiate(hard, new Vector3(0, 0, 0), Quaternion.identity);

        gameObject.SetActive(false);
    }
}
